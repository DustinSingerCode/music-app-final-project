let express = require('express');
let app = express();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let cors = require('cors');

app.use(cors());

//Data model:
//let rooms = {roomName: {password: 1234, votes: 0, hostSocketID: nowPlaying: {}, users: ["user 1", "user 2"], songsInQueue: [ {songObject with username}, {}]

let rooms = {}

io.on('connection', (socket) => {
    console.log("A user connected");

    socket.on('disconnect', () => {
        if(rooms[socket.roomName] === undefined){
            return;
        }
        else if(socket.id === rooms[socket.roomName].hostSocketID){
            io.to(socket.roomName).emit('hostLeft', true);
            delete rooms[socket.roomName];
            console.log('room deleted')
        }
        console.log("user has disconnected");
        
    })


    socket.on('createRoom', (room) => {
        if (rooms[room.roomName]) {
            socket.emit('createRoom', { success: false, message: "A room with the same name already exists" })
        }
        else {
            rooms[room.roomName] = {};
            rooms[room.roomName].password = room.password;
            rooms[room.roomName].hostSocketID = socket.id;
            socket.roomName = room.roomName;
            socket.join(room.roomName);
            socket.emit('createRoom', { success: true, roomName: room.roomName })
        }
    })

    socket.on('joinRoom', (room) => {
        if (rooms[room.roomName] && rooms[room.roomName].password === room.password) {
            if (rooms[room.roomName].songsInQueue === undefined) {
                rooms[room.roomName].songsInQueue = [];
            }
            if (rooms[room.roomName].nowPlaying === undefined){
                rooms[room.roomName].nowPlaying = {title: ''}
            }
            socket.roomName = room.roomName;
            socket.join(room.roomName);
            socket.emit('joinRoom', {success: true, nowPlaying: rooms[room.roomName].nowPlaying, roomName: room.roomName});
            socket.emit('returnSongs', rooms[room.roomName].songsInQueue)
            console.log('user joined room: ', room.roomName);
        }
        else {
            socket.emit('joinRoom', false);
            console.log('room does not exist');
        }

    })

    socket.on('setUsername', (req) => {
        let username = req.username;
        let roomName = req.roomName;
        let exists = false;

        if (rooms[roomName].users === undefined) {
            rooms[roomName].users = []
            rooms[roomName].users = rooms[roomName].users.concat(username);
            socket.emit('usernameSet', { success: true });
            io.to(roomName).emit('getUsers', rooms[roomName].users);
        }
        else {
            for (let i = 0; i < rooms[roomName].users.length; i++) {
                if (rooms[roomName].users[i] === username) {
                    exists = true;
                    socket.emit('usernameSet', { success: false, message: "User already exists, please choose a different username" })
                }
            }
            if (exists === false) {
                rooms[roomName].users = rooms[roomName].users.concat(username);
                socket.emit('usernameSet', { success: true });
                io.to(roomName).emit('getUsers', rooms[roomName].users);
            }
        }

    })

    socket.on('firstSong', (req) => {
        rooms[req.roomName].nowPlaying = req.nowPlaying;
        io.to(req.roomName).emit('nowPlaying', rooms[req.roomName].nowPlaying);
    })

    socket.on('updateSongs', (req) => {
        rooms[req.roomName].songsInQueue.splice(0, 1);
        rooms[req.roomName].nowPlaying = req.nowPlaying;
        io.to(req.roomName).emit('nowPlaying', rooms[req.roomName].nowPlaying);
        io.to(req.roomName).emit('addSong', rooms[req.roomName].songsInQueue);
        
    })

    socket.on('addSong', (req) => {
        let selectedVideo = req.selectedVideo;
        let roomName = req.roomName;
        let username = req.username;
        let exists = false;
        let count = 0;

        if (rooms[roomName].songsInQueue === undefined) {
            rooms[roomName].songsInQueue = [];
            selectedVideo.username = username;
            rooms[roomName].songsInQueue = rooms[roomName].songsInQueue.concat(selectedVideo);
            io.to(roomName).emit('addSong', rooms[roomName].songsInQueue);
        }
        else {
            for (let i = 0; i < rooms[roomName].songsInQueue.length; i++) {
                if (rooms[roomName].songsInQueue[i].id === selectedVideo.id) {
                    exists = true;
                    socket.emit('addSongError', "Song already exists in queue")
                }
                else if(rooms[roomName].songsInQueue[i].username === username){
                    count++
                }
            }
            if(count === 2){
                socket.emit('addSongError', "You already have two songs in queue")
            }
            else if (exists === false) {
                selectedVideo.username = username;
                rooms[roomName].songsInQueue = rooms[roomName].songsInQueue.concat(selectedVideo);
                io.to(roomName).emit('addSong', rooms[roomName].songsInQueue);
            }
        }
    });

    socket.on('vote', roomName => {
        if(rooms[roomName].votes === undefined){
            rooms[roomName].votes = 1;  
            io.to(roomName).emit('returnVotes', rooms[roomName].votes); 
            console.log(rooms[roomName].votes)
        }
        else{
            rooms[roomName].votes++;
            if(rooms[roomName].votes > rooms[roomName].users.length/2){
                io.to(rooms[roomName].hostSocketID).emit('vote');
                rooms[roomName].votes = 0;
                io.to(roomName).emit('returnVotes', rooms[roomName].votes); 
                rooms[roomName].votes = undefined;

            }
            else{
                io.to(rooms[roomName]).emit('returnVotes', rooms[roomName].votes);
            }
        }
    })

    socket.on('leaveRoom', (req) => {
        if (rooms === {}) { 
            return;
        }
        else {
            if(rooms[req.roomName] === undefined){
                return;
            }
            else if(rooms[req.roomName].users === undefined){
                return;
            }
            for (let i = 0; i < rooms[req.roomName].users.length; i++) {
                if (rooms[req.roomName].users[i] === req.username) {
                    rooms[req.roomName].users.splice(i, 1);
                    io.to(req.roomName).emit('getUsers', rooms[req.roomName].users);
                    break;
                }
            }
        }
    })
})

http.listen(4000, () => {
    console.log("Listening on port 4000");
});