import React, {Component} from 'react';
import Search from 'youtube-search';
import Youtube from 'react-youtube';

class Queue extends Component{
    constructor(props){
        super(props);

        this.state = {
            videoID: '',
            songsInQueue: []
        }

    }

    addToQueue(e, selectedVideo) {
        e.preventDefault();
        if (this.state.firstSong === true && this.state.identity === 'host') {
          this.setState({ videoID: selectedVideo.id, firstSong: false, searchQuery: '', searchResults: [] })
          this.props.socket.emit('firstSong', { nowPlaying: selectedVideo, roomName: this.state.roomName });
        }
        else {
          this.props.socket.emit('addSong', { selectedVideo: selectedVideo, roomName: this.state.roomName, username: this.state.username });
          this.setState({searchQuery: '', searchResults: []});
        }
      }

    playNext(){

    }


    componentDidMount(){
        this.props.socket.on('addSong', (songs) => {
            this.setState({ songsInQueue: songs });
          });

          this.props.socket.on('returnSongs', (songs) => {
            this.setState({ songsInQueue: songs });
          });

          this.props.socket.on('addSongError', (message) => {
            console.log(message);
          });
        
    }

    render(){
        return(   
        <div>
            <div id="songSearch">
            Add a song: 
            <input type="text" onChange={this.handleSearch} value={this.state.searchQuery} />
            <br></br>
            </div>
            <h2>Songs in queue:</h2>
            <div> {this.state.songsInQueue.map((x, i) => <div key={i}>{x.title}</div>)}</div>
        </div>
        );

    }

}

export default Queue;