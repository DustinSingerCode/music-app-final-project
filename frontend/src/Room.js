import React, { Component } from 'react';
import { Tabs, Tab } from 'react-bootstrap';
import anime from 'animejs';
import './App.css';
import './Room.css';
import Search from 'youtube-search';
import YouTube from 'react-youtube';


let animateConfirmUsernameButton = () => {
  anime({
    targets: document.getElementById("confirmUsername"),
    scale: 1.1,
    duration: 1000,
    easing: 'easeOutCirc',
    backgroundColor: '#ff0000'
  })
}

const opts = {
  maxResults: 10,
  videoEmbeddable: true,
  videoSyndicated: true,
  type: "video",
  key: "AIzaSyBRncw-D-Jgk8Q_oxUWbTfHiBKk-K_lqdk"
}

const playerOpts = {
  playerVars: { // https://developers.google.com/youtube/player_parameters
    autoplay: 1,
  }
}

class Room extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchQuery: '',
      videoID: '',
      songsInQueue: [],
      roomName: this.props.roomName,
      searchResults: [],
      username: '',
      usernameSet: false,
      usersInRoom: [],
      firstSong: true,
      identity: this.props.identity,
      nowPlaying: this.props.nowPlaying,
      hostLeft: false,
      votes: 0,
      voteButtonDisabled: false,
      day: new Date().toDateString(),
      time: new Date().getHours().toString() + ":" + ("0" + new Date().getMinutes()).toString().slice(-2)
    }
    this.handleSearch = this.handleSearch.bind(this);
    this.addToQueue = this.addToQueue.bind(this);
    this.onError = this.onError.bind(this);
    this.handleUsername = this.handleUsername.bind(this);
    this.setUsername = this.setUsername.bind(this);
    this.playNext = this.playNext.bind(this);
    this.ytSearch = this.ytSearch.bind(this);
    this.voteToSkip = this.voteToSkip.bind(this);
  }

  componentDidMount() {
    this.props.socket.on('hostLeft', res => {
      this.setState({ hostLeft: res });
    })

    this.props.socket.on('vote', () => {
      this.playNext();
    })

    this.props.socket.on('returnVotes', votes => {
      console.log(votes);
      this.setState({ votes: votes })
    })

    this.props.socket.on('nowPlaying', nowPlaying => {
      this.setState({ nowPlaying: nowPlaying })
    })

    this.props.socket.on('addSong', (songs) => {
      this.setState({ songsInQueue: songs, voteButtonDisabled: false });
    });

    this.props.socket.on('returnSongs', (songs) => {
      this.setState({ songsInQueue: songs });
    });

    this.props.socket.on('addSongError', (message) => {
      console.log(message);
    });

    this.props.socket.on('usernameSet', (res) => {
      if (res.success === true) {
        this.setState({ usernameSet: true });
      }
      else {
        console.log(res.message)
      }
    })

    this.props.socket.on('getUsers', (res) => {
      this.setState({ usersInRoom: res })
    })

    window.addEventListener('unload', (ev) => {
      this.props.socket.emit('leaveRoom', { username: this.state.username, roomName: this.state.roomName });
    })
  }

  addToQueue(e, selectedVideo) {
    e.preventDefault();
    if (this.state.firstSong === true && this.state.identity === 'host') {
      this.setState({ videoID: selectedVideo.id, firstSong: false, searchQuery: '', searchResults: [] })
      this.props.socket.emit('firstSong', { nowPlaying: selectedVideo, roomName: this.state.roomName });
    }
    else {
      this.props.socket.emit('addSong', { selectedVideo: selectedVideo, roomName: this.state.roomName, username: this.state.username });
      this.setState({ searchQuery: '', searchResults: [] });
    }
  }

  playNext() {
    if (this.state.songsInQueue != 0) {
      let nextSong = this.state.songsInQueue[0];
      this.setState({ videoID: nextSong.id, voteButtonDisabled: false });
      this.props.socket.emit('updateSongs', { roomName: this.state.roomName, songsInQueue: this.state.songsInQueue, nowPlaying: nextSong });
    }
  }

  ytSearch(e) {
    if (e.target.value === '') {
      this.setState({ searchResults: [] })
    }
    else {
      Search(e.target.value, opts, (err, searchResults) => {
        searchResults = searchResults.filter(x => x.id.length === 11)
        this.setState({ searchResults: searchResults })
      });
    }
  }

  voteToSkip(e) {
    e.preventDefault();
    this.setState({ voteButtonDisabled: true });
    this.props.socket.emit('vote', this.state.roomName);
  }

  handleSearch(e) {
    this.setState({ searchQuery: e.target.value })
    this.ytSearch(e);
  }

  handleUsername(e) {
    this.setState({ username: e.target.value });
  }

  setUsername(e) {
    e.preventDefault();
    this.props.socket.emit('setUsername', { username: this.state.username, roomName: this.state.roomName });
  }

  onError(e) {
    console.log(e)
  }

  render() {
    if (this.state.hostLeft === true) {
      return (
        <div className='App'>
          <h1>Host has left, room closed!</h1>
        </div>
      )
    }

    else if (this.state.usernameSet === false) {
      return (
        <div className='App' id="usernameForm">
          <form onSubmit={this.setUsername} id="usernameForm">
            <p id="content">Enter Username</p>
            <input type="text" onChange={this.handleUsername} id="enterUsername" />
            <input type="submit" value="Submit" id="confirmUsername" />
          </form>

          {
            document.addEventListener("DOMContentLoaded", function () {
              document.getElementById("confirmUsername").addEventListener("click", function () {
                animateConfirmUsernameButton();
              })
            })
          }

        </div>
      )
    }
    if (this.state.identity === 'host') {
      return (
        <div className="App">
          <div className="headerRoom">
            <ul>
              <li className="motif">M<div className="second-letter">o</div>tif</li>
              <li className="motif-sub">- The Spirit of Group Music-</li>
            </ul>
          </div>
          <br></br>
          <br></br>
          <div id="welcomeOne">
            <ul>
              <li>Welcome to your new room <br /> {this.state.username.charAt(0).toUpperCase() + this.state.username.substr(1).toLowerCase()}</li>
              <li>Room Name - {this.state.roomName}</li>
              <li>Date of Creation -  {this.state.day + " at " + this.state.time}</li>
              <li>Number of Guests - {this.state.usersInRoom.length}</li>
            </ul>

          </div>
          <div>
            <div className="songSearch"></div>
            <div className="addSongField">
              <input type="text" onChange={this.handleSearch} value={this.state.searchQuery} placeholder="Add a Song" />
              <p>&#128269;</p>
            </div>
            <br></br>
          </div>
          <div className='search-container'>{this.state.searchResults.map((x, i) =>
            <div>
              <button onClick={(e) => this.addToQueue(e, x)} className="search-results" key={i}>
                <div>
                  <div>
                    <img className="search-image" src={x.thumbnails.default.url}></img>
                  </div>
                  <div>{x.title}</div>
                </div>
              </button>
            </div>)}
          </div>
          <div className="videoContainer">
            <YouTube videoId={this.state.videoID} opts={playerOpts} onReady={this._onReady} onError={this.onError} onEnd={this.playNext} />
          </div>
          <br></br>
          <div className="videoDetails">
            <p className="videoNowPlaying">{this.state.nowPlaying.title}</p>
          </div>
          <div className="votingContainer">
            <div style={{ color: "#040426" }}>{this.state.votes + " people voted to skip this song"}</div>
            <button onClick={this.voteToSkip} disabled={this.state.voteButtonDisabled} className="skipButton">Vote to Skip</button>
          </div>

          <br></br>
          <div className="tabsContainer">
            <Tabs defaultActiveKey={1} id="tabs">
              <Tab eventKey={1} title="Queue">
                <div className="tabsContent">
                  <p className="songsQueue">Songs in Queue</p>
                  <div> {this.state.songsInQueue.map((x, i) => <div className="song-list" key={i}>{x.title}</div>)}</div>
                </div>
              </Tab>
              <Tab eventKey={2} title="Users">
                <div className="tabsContent">
                  <p className="usersRoom">Users in Room</p>
                  <div>{this.state.usersInRoom.map((x, i) => <div className="song-list" key={i}>{x}</div>)}</div>
                </div>
              </Tab>
            </Tabs>
          </div>
        </div>
      );
    }
    else {
      return (
        <div className="App">
          <div className="headerRoom">
            <ul>
              <li className="motif">Motif</li>
              <li className="motif-sub">- The Spirit of Group Music-</li>
            </ul>
          </div>
          <br></br>
          <br></br>
          <div id="welcomeOne">
            <ul>
              <li>Welcome to your new room <br /> {this.state.username.charAt(0).toUpperCase() + this.state.username.substr(1).toLowerCase()}</li>
              <li>Room Name - {this.state.roomName}</li>
              <li>Date of Creation -  {this.state.day + " at " + this.state.time}</li>
              <li>Number of Guests - {this.state.usersInRoom.length}</li>
            </ul>

          </div>
          <div>
            <div className="songSearch"></div>
            <div className="addSongField">
              <input type="text" onChange={this.handleSearch} value={this.state.searchQuery} placeholder="Add a Song" />
              <p>&#128269;</p>
            </div>
            <br></br>
          </div>
          <div className='search-container'>{this.state.searchResults.map((x, i) =>
            <div>
              <button onClick={(e) => this.addToQueue(e, x)} className="search-results" key={i}>
                <div>
                  <div>
                    <img className="search-image" src={x.thumbnails.default.url}></img>
                  </div>
                  <div>{x.title}</div>
                </div>
              </button>
            </div>)}
          </div>
          <br></br>
          <div className="videoDetails">
            <p className="videoNowPlayingClient">Now Playing: {this.state.nowPlaying.title}</p>
          </div>
          <div className="votingContainerClient">
            <div style={{ color: "#040426" }}>{this.state.votes + " people voted to skip this song"}</div>
            <button onClick={this.voteToSkip} disabled={this.state.voteButtonDisabled} className="skipButton">Vote to Skip</button>
          </div>

          <br></br>
          <div className="tabsContainer">
            <Tabs defaultActiveKey={1} id="tabs">
              <Tab eventKey={1} title="Queue">
                <div className="tabsContent">
                  <p className="songsQueue">Songs in Queue</p>
                  <div> {this.state.songsInQueue.map((x, i) => <div className="song-list" key={i}>{x.title}</div>)}</div>
                </div>
              </Tab>
              <Tab eventKey={2} title="Users">
                <div className="tabsContent">
                  <p className="usersRoom">Users in Room</p>
                  <div>{this.state.usersInRoom.map((x, i) => <div className="song-list" key={i}>{x}</div>)}</div>
                </div>
              </Tab>
            </Tabs>
          </div>
        </div>
      );
    }
  }
}

export default Room;
