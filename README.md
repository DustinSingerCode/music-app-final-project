This is a real time collaborative music player where different users can join the same room, add songs, and vote to skip to the next song.

Here's a video demo of the feature: https://www.youtube.com/watch?v=JpUHTpX1HRc